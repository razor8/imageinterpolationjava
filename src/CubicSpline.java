import java.util.Arrays;

/**
 * Die Klasse CubicSpline bietet eine Implementierung der kubischen Splines. Sie
 * dient uns zur effizienten Interpolation von aequidistanten Stuetzpunkten.
 * 
 * @author braeckle
 * 
 */
public class CubicSpline implements InterpolationMethod {

	/** linke und rechte Intervallgrenze x[0] bzw. x[n] */
	double a, b;

	/** Anzahl an Intervallen */
	int n;

	/** Intervallbreite */
	double h;

	/** Stuetzwerte an den aequidistanten Stuetzstellen */
	double[] y;

	/** zu berechnende Ableitunge an den Stuetzstellen */
	double yprime[];

	/**
	 * {@inheritDoc} Zusaetzlich werden die Ableitungen der stueckweisen
	 * Polynome an den Stuetzstellen berechnet. Als Randbedingungen setzten wir
	 * die Ableitungen an den Stellen x[0] und x[n] = 0.
	 */
	@Override
	public void init(double a, double b, int n, double[] y) {
		this.a = a;
		this.b = b;
		this.n = n;
		h = ((double) b - a) / (n);

		this.y = Arrays.copyOf(y, n + 1);

		/* Randbedingungen setzten */
		yprime = new double[n + 1];
		yprime[0] = 0;
		yprime[n] = 0;

		/* Ableitungen berechnen. Nur noetig, wenn n > 1 */
		if (n > 1) {
			computeDerivatives();
		}
	}

	/**
	 * getDerivatives gibt die Ableitungen yprime zurueck
	 */
	public double[] getDerivatives() {
		return yprime;
	}

	/**
	 * Setzt die Ableitungen an den Raendern x[0] und x[n] neu auf yprime0 bzw.
	 * yprimen. Anschliessend werden alle Ableitungen aktualisiert.
	 */
	public void setBoundaryConditions(double yprime0, double yprimen) {
		yprime[0] = yprime0;
		yprime[n] = yprimen;
		if (n > 1) {
			computeDerivatives();
		}
	}

	/**
	 * Berechnet die Ableitungen der stueckweisen kubischen Polynome an den
	 * einzelnen Stuetzstellen. Dazu wird ein lineares System Ax=c mit einer
	 * Tridiagonalen Matrix A und der rechten Seite c aufgebaut und geloest.
	 * Anschliessend sind die berechneten Ableitungen y1' bis yn-1' in der
	 * Membervariable yprime gespeichert.
	 * 
	 * Zum Zeitpunkt des Aufrufs stehen die Randbedingungen in yprime[0] und yprime[n].
	 * Speziell bei den "kleinen" Faellen mit Intervallzahlen n = 2
	 * oder 3 muss auf die Struktur des Gleichungssystems geachtet werden. Der
	 * Fall n = 1 wird hier nicht beachtet, da dann keine weiteren Ableitungen
	 * berechnet werden muessen.
	 */
	public void computeDerivatives() {
		/* TODO: diese Methode ist zu implementieren */
		int neo =n-1;
		double[] v=new double [neo];
		double[] b=new double [neo];
		for(int i=0;i<neo;i++){
			b[i]=4;
		}
		
		for(int i=0;i<neo-2;i++){
			
			v[i]=(3/h)*(y[i+2]-y[i]);
			
			
		}
		
		// abgewandelter Thomas-Algorithmus
        for (int i = 1; i < neo; i++)
        {
                double m = 1/b[i-1];
                b[i] = b[i] - m ;
                v[i] = v[i] - m*v[i-1];
        }

        yprime[neo-1] = v[neo-1]/b[neo-1];

        for (int i = neo - 2; i >= 0; --i) {
                yprime[i] = (v[i] -   yprime[i+1]) / b[i];
        }
		
		
	}

	/**
	 * {@inheritDoc} Liegt z ausserhalb der Stuetzgrenzen, werden die
	 * aeussersten Werte y[0] bzw. y[n] zurueckgegeben. Liegt z zwischen den
	 * Stuetzstellen x_i und x_i+1, wird z in das Intervall [0,1] transformiert
	 * und das entsprechende kubische Hermite-Polynom ausgewertet.
	 */
	@Override
	public double evaluate(double z) {
		/* TODO: diese Methode ist zu implementieren */
		if(z < y[0]) return y[0];                        //
		if(z > y[y.length-1]) return y[y.length-1];
		
		int interv = (int) ((z - a) / h);   //? intervall finden
		
		double t=(double)(z-a)/h;   //(?) xi nicht a  !!  (interv-xi)/h          (x-xi)/h
		
		
		
		int m=interv; // i   ?
		return y[m]*(1-3*t*t+2*t*t*t)+y[m+1]*t*t*(3-2*t)+h*yprime[m]*(t-2*t*t+t*t*t)+h*yprime[m+1]*(-t*t+t*t*t);
		
		//return 0.0;
	}
}
